﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationProjectForBLPFS.Context.Model
{
    public class EquipmentAssignment
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public DateTime AssingTimeUTC { get; set; }
        public DateTime DeAssingTimeUTC { get; set; }
        public bool Deleted { get; set; }
        public int PropertyId { get; set; }
        public virtual Property Property { get; set; }
        public int PremiseId { get; set; }
        public virtual Premise Premise { get; set; }
        public int EquipmentId { get; set; }
        public virtual Equipment Equipment { get; set; }
    }
}
