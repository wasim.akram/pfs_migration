﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationProjectForBLPFS.Context.Model
{
    public class Equipment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EUSTTypeId { get; set; }
        public string Status { get; set; }
        public bool Deleted { get; set; }
        public int PropertyId { get; set; }
        public virtual Property Property { get; set; }
        public int PremiseId { get; set; }
        public virtual Premise Premise { get; set; }
        public virtual ICollection<EquipmentAssignment> EquipmentAssignments { get; set; }
        public virtual ICollection<EquipmentSpecification> EquipmentSpecifications { get; set; }
    }
}
