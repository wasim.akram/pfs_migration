﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationProjectForBLPFS.Context.Model
{
    public class ZoneCourier
    {
        public int Id { get; set; }
        public int SLA { get; set; }
        public double Charge { get; set; }
        public int Number { get; set; }
        public string Address { get; set; }
        public bool Deleted { get; set; }
        public int ZoneId { get; set; }
        public virtual Zone Zone { get; set; }
        public int CourierId { get; set; }
        public virtual Courier Courier { get; set; }
        
    }
}
