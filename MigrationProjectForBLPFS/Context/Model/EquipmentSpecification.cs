﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationProjectForBLPFS.Context.Model
{
    public class EquipmentSpecification
    {
        public int Id { get; set; }
        public double Value { get; set; }
        public bool Deleted { get; set; }
        public int EquipmentId { get; set; }
        public virtual Equipment Equipment { get; set; }
        public int SpecificationId { get; set; }
        public virtual Specification Specification { get; set; }
    }
}
