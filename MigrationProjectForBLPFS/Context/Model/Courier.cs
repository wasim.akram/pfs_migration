﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationProjectForBLPFS.Context.Model
{
    public class Courier
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public virtual ICollection<ZoneCourier> ZoneCouriers { get; set; }
    }
}
