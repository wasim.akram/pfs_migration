﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationProjectForBLPFS.Context.Model
{
    public class Property
    {
        public int Id { get; set; }
        public virtual ICollection<Equipment> Equipments { get; set; }
        public virtual ICollection<EquipmentAssignment> EquipmentAssignments { get; set; }
    }
}
