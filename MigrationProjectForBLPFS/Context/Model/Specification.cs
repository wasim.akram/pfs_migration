﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationProjectForBLPFS.Context.Model
{
    public class Specification
    {
        public int Id { get; set; }
        public string SpecificationName { get; set; }
        public int EUSTTypeId { get; set; }
        public string Unit { get; set; }
        public bool Deleted { get; set; }
        public virtual ICollection<EquipmentSpecification> EquipmentSpecifications { get; set; }
    }
}
