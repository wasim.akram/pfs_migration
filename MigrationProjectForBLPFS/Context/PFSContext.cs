﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MigrationProjectForBLPFS.Context.Config;
using MigrationProjectForBLPFS.Context.Model;

namespace MigrationProjectForBLPFS.Context
{
    public class PFSContext : DbContext
    {
        public PFSContext(DbContextOptions<PFSContext> options) : base(options)
        {

        }

        public virtual DbSet<SequenceTracker> SequenceTrackers { get; set; }
        public virtual DbSet<Zone> Zones { get; set; }
        public virtual DbSet<Courier> Couriers { get; set; }
        public virtual DbSet<ZoneCourier> ZoneCouriers { get; set; }
        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<Premise> Premises { get; set; }
        public virtual DbSet<Equipment> Equipments { get; set; }
        public virtual DbSet<Specification> Specifications { get; set; }
        public virtual DbSet<EquipmentSpecification> EquipmentSpecifications { get; set; }
        public virtual DbSet<EquipmentAssignment> EquipmentAssignments { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ZoneConfig());
            modelBuilder.ApplyConfiguration(new CourierConfig());
            modelBuilder.ApplyConfiguration(new ZoneCourierConfig());
            modelBuilder.ApplyConfiguration(new PropertyConfig());
            modelBuilder.ApplyConfiguration(new PremiseConfig());
            modelBuilder.ApplyConfiguration(new EqipmentConfig());
            modelBuilder.ApplyConfiguration(new SpecificationConfig());
            modelBuilder.ApplyConfiguration(new EquipmentAssignmentConfig());
            modelBuilder.ApplyConfiguration(new EquipmentSpecificationConfig());
        }

    }

    public class SequenceTracker
    {
        public int id { get;set; }
        public string Name { get; set; }
    }
}

