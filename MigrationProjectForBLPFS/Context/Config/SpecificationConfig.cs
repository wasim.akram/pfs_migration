﻿using MigrationProjectForBLPFS.Context.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MigrationProjectForBLPFS.Context.Config
{
    public class SpecificationConfig : IEntityTypeConfiguration<Specification>
    {
        public void Configure(EntityTypeBuilder<Specification> builder)
        {
            builder.ToTable(nameof(Specification));
            builder.HasKey(x => x.Id);
            builder.Property(p => p.Deleted)
               .HasDefaultValue(false);
        }
    }
}
