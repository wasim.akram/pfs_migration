﻿using MigrationProjectForBLPFS.Context.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MigrationProjectForBLPFS.Context.Config
{
    public class ZoneCourierConfig : IEntityTypeConfiguration<ZoneCourier>
    {
        public void Configure(EntityTypeBuilder<ZoneCourier> builder)
        {
            builder.ToTable("ZoneCourier");

            builder.HasKey(zc =>new { zc.Id,zc.ZoneId,zc.CourierId} );

            builder.HasOne(zc => zc.Zone)
                .WithMany(z => z.ZoneCouriers)
                .HasForeignKey(zc => zc.ZoneId);
            builder.HasOne(zc => zc.Courier)
                .WithMany(z => z.ZoneCouriers)
                .HasForeignKey(zc => zc.CourierId);

        }
    }
}
