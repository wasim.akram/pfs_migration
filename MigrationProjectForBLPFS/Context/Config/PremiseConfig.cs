﻿using MigrationProjectForBLPFS.Context.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MigrationProjectForBLPFS.Context.Config
{
    public class PremiseConfig : IEntityTypeConfiguration<Premise>
    {
        public void Configure(EntityTypeBuilder<Premise> builder)
        {
            builder.ToTable(nameof(Premise));
            builder.HasKey(x => x.Id);
        }
    }
}
