﻿using MigrationProjectForBLPFS.Context.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MigrationProjectForBLPFS.Context.Config
{
    public class EquipmentSpecificationConfig : IEntityTypeConfiguration<EquipmentSpecification>
    {
        public void Configure(EntityTypeBuilder<EquipmentSpecification> builder)
        {
            builder.ToTable(nameof(EquipmentSpecification));
            builder.HasKey(x =>x.Id );
            builder.Property(p => p.Deleted)
               .HasDefaultValue(false);
            builder.HasOne(m => m.Equipment)
               .WithMany(p => p.EquipmentSpecifications)
               .HasForeignKey(m => m.EquipmentId);
            builder.HasOne(m => m.Specification)
               .WithMany(p => p.EquipmentSpecifications)
               .HasForeignKey(m => m.SpecificationId);
        }
    }
}
