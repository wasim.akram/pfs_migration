﻿using MigrationProjectForBLPFS.Context.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MigrationProjectForBLPFS.Context.Config
{
    public class CourierConfig : IEntityTypeConfiguration<Courier>
    {
        public void Configure(EntityTypeBuilder<Courier> builder)
        {
            builder.ToTable("Courier");

            builder.HasKey(x => x.Id);

        }
    }
}
