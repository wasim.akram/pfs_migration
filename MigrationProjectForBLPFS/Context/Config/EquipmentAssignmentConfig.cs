﻿using MigrationProjectForBLPFS.Context.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MigrationProjectForBLPFS.Context.Config
{
    public class EquipmentAssignmentConfig : IEntityTypeConfiguration<EquipmentAssignment>
    {
        public void Configure(EntityTypeBuilder<EquipmentAssignment> builder)
        {
            builder.ToTable(nameof(EquipmentAssignment));
            builder.HasKey(x => x.Id);
            builder.HasOne(m => m.Property)
                .WithMany(p => p.EquipmentAssignments)
                .HasForeignKey(m => m.PropertyId);
            builder.HasOne(m => m.Premise)
               .WithMany(p => p.EquipmentAssignments)
               .HasForeignKey(m => m.PremiseId);
            builder.HasOne(m => m.Equipment)
               .WithMany(p => p.EquipmentAssignments)
               .HasForeignKey(m => m.EquipmentId);
            builder.Property(p => p.Deleted)
                .HasDefaultValue(false);
        }
    }
}
