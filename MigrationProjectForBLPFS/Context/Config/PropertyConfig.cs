﻿using MigrationProjectForBLPFS.Context.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MigrationProjectForBLPFS.Context.Config
{
    public class PropertyConfig : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> builder)
        {
            builder.ToTable(nameof(Property));
            builder.HasKey(x => x.Id);
        }
    }
}
