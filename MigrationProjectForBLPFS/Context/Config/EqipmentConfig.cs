﻿using MigrationProjectForBLPFS.Context.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MigrationProjectForBLPFS.Context.Config
{
    public class EqipmentConfig : IEntityTypeConfiguration<Equipment>
    {
        public void Configure(EntityTypeBuilder<Equipment> builder)
        {
            builder.ToTable(nameof(Equipment));
            builder.HasKey(x => x.Id);
            builder.HasOne(m => m.Property)
                .WithMany(p =>p.Equipments)
                .HasForeignKey(m => m.PropertyId);
            builder.HasOne(m => m.Premise)
               .WithMany(p => p.Equipments)
               .HasForeignKey(m => m.PremiseId);
            builder.Property(p => p.Deleted)
                .HasDefaultValue(false);

        }
    }
}
