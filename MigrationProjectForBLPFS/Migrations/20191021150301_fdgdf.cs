﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MigrationProjectForBLPFS.Migrations
{
    public partial class fdgdf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AssingTimeUTC",
                table: "EquipmentAssignment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeAssingTimeUTC",
                table: "EquipmentAssignment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssingTimeUTC",
                table: "EquipmentAssignment");

            migrationBuilder.DropColumn(
                name: "DeAssingTimeUTC",
                table: "EquipmentAssignment");
        }
    }
}
