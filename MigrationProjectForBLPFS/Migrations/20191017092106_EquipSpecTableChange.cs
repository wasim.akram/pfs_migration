﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MigrationProjectForBLPFS.Migrations
{
    public partial class EquipSpecTableChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Courier",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true),
                    Deleted = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courier", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Premise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Premise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Property",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Property", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SequenceTrackers",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SequenceTrackers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Specification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    SpecificationName = table.Column<string>(nullable: true),
                    EUSTTypeId = table.Column<int>(nullable: false),
                    Unit = table.Column<string>(nullable: true),
                    Deleted = table.Column<short>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Zone",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true),
                    Deleted = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zone", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Equipment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Name = table.Column<string>(nullable: true),
                    EUSTTypeId = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    Deleted = table.Column<short>(nullable: false, defaultValue: false),
                    PropertyId = table.Column<int>(nullable: false),
                    PremiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Equipment_Premise_PremiseId",
                        column: x => x.PremiseId,
                        principalTable: "Premise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Equipment_Property_PropertyId",
                        column: x => x.PropertyId,
                        principalTable: "Property",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ZoneCourier",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    SLA = table.Column<int>(nullable: false),
                    Charge = table.Column<double>(nullable: false),
                    Number = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Deleted = table.Column<short>(nullable: false),
                    ZoneId = table.Column<int>(nullable: false),
                    CourierId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZoneCourier", x => new { x.Id, x.ZoneId, x.CourierId });
                    table.ForeignKey(
                        name: "FK_ZoneCourier_Courier_CourierId",
                        column: x => x.CourierId,
                        principalTable: "Courier",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ZoneCourier_Zone_ZoneId",
                        column: x => x.ZoneId,
                        principalTable: "Zone",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EquipmentAssignment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Status = table.Column<string>(nullable: true),
                    Deleted = table.Column<short>(nullable: false, defaultValue: false),
                    PropertyId = table.Column<int>(nullable: false),
                    PremiseId = table.Column<int>(nullable: false),
                    EquipmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentAssignment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EquipmentAssignment_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "Equipment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EquipmentAssignment_Premise_PremiseId",
                        column: x => x.PremiseId,
                        principalTable: "Premise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EquipmentAssignment_Property_PropertyId",
                        column: x => x.PropertyId,
                        principalTable: "Property",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EquipmentSpecification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Value = table.Column<double>(nullable: false),
                    Deleted = table.Column<short>(nullable: false, defaultValue: false),
                    EquipmentId = table.Column<int>(nullable: false),
                    SpecificationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentSpecification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EquipmentSpecification_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "Equipment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EquipmentSpecification_Specification_SpecificationId",
                        column: x => x.SpecificationId,
                        principalTable: "Specification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Equipment_PremiseId",
                table: "Equipment",
                column: "PremiseId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipment_PropertyId",
                table: "Equipment",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentAssignment_EquipmentId",
                table: "EquipmentAssignment",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentAssignment_PremiseId",
                table: "EquipmentAssignment",
                column: "PremiseId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentAssignment_PropertyId",
                table: "EquipmentAssignment",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentSpecification_EquipmentId",
                table: "EquipmentSpecification",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentSpecification_SpecificationId",
                table: "EquipmentSpecification",
                column: "SpecificationId");

            migrationBuilder.CreateIndex(
                name: "IX_ZoneCourier_CourierId",
                table: "ZoneCourier",
                column: "CourierId");

            migrationBuilder.CreateIndex(
                name: "IX_ZoneCourier_ZoneId",
                table: "ZoneCourier",
                column: "ZoneId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EquipmentAssignment");

            migrationBuilder.DropTable(
                name: "EquipmentSpecification");

            migrationBuilder.DropTable(
                name: "SequenceTrackers");

            migrationBuilder.DropTable(
                name: "ZoneCourier");

            migrationBuilder.DropTable(
                name: "Equipment");

            migrationBuilder.DropTable(
                name: "Specification");

            migrationBuilder.DropTable(
                name: "Courier");

            migrationBuilder.DropTable(
                name: "Zone");

            migrationBuilder.DropTable(
                name: "Premise");

            migrationBuilder.DropTable(
                name: "Property");
        }
    }
}
